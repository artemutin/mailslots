#include "common_header.h"
#include "MailslotClient.h"

	MailslotClient::MailslotClient(std::string server, std::string path)
	{
		setServer(server);
		setPath(path);
	}

	void MailslotClient::setServer(const std::string server){
		this->server = server;
	}

	void MailslotClient::setPath(const std::string path){
		this->path = path;
	}

	bool MailslotClient::sendMessage(std::string message){
		std::string mail_address = "\\\\" + server + "\\mailslot\\" + path;

		HANDLE file_h = CreateFileA(mail_address.c_str(), GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
		if (file_h == INVALID_HANDLE_VALUE){
        std::ostringstream out;
        out << "Error while creating file num " << GetLastError() << std::endl;
        throw CreatingFileException(out.str());
		}

		DWORD num_bytes_read;
		BOOL fResult = WriteFile(file_h, message.c_str(), (message.length() + 1)*sizeof(char), &num_bytes_read, NULL);
		if (!fResult) 
		{ 
            std::ostringstream out;
            out << "Writing to mailslot failed with %d.\n" << GetLastError() << std::endl;
            throw WritingMailslotException(out.str());
			return false; 
		} 

		CloseHandle(file_h);
		return true;
	}

