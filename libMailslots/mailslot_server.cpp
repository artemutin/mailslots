#include "common_header.h"
#include "MailslotServer.h"


	MailslotServer::MailslotServer(std::string server, std::string path, DWORD buf_len)
	{
		slot_h = INVALID_HANDLE_VALUE;
		std::string mail_address = "\\\\" + server + "\\mailslot\\" + path;
		slot_h = CreateMailslotA(mail_address.c_str(), buf_len, MAILSLOT_WAIT_FOREVER, NULL);
		if (slot_h == INVALID_HANDLE_VALUE){
            std::ostringstream out;
            out << "Error creating mailslot num " << GetLastError() << std::endl;
            throw CreatingMailslotException(out.str());
		}
	}

	HANDLE MailslotServer::get_slot()
	{
		return slot_h;
	}

    std::string MailslotServer::read_message()
	{
		if (slot_h == INVALID_HANDLE_VALUE){
            std::ostringstream out;
            out << "Error reading mailslot num " << GetLastError() << std::endl;
            throw ReadingMailslotException(out.str());
		}

		DWORD cbMessage, cMessage, cbRead; 
		 BOOL fResult = GetMailslotInfo( slot_h, // mailslot handle 
        (LPDWORD) NULL,               // no maximum message size 
        &cbMessage,                   // size of next message 
        &cMessage,                    // number of messages 
        (LPDWORD) NULL);              // no read time-out 
 
		if (!fResult) 
		{ 
            std::ostringstream out;
            out << "GetMailslotInfo failed with %d.\n" << GetLastError() << std::endl;
            throw NotifierException(out.str());
            return "";
		} 

		if (cbMessage == MAILSLOT_NO_MESSAGE) 
		{ 
            throw NotifierException("Waiting for a message...\n");
            return "";
		} 
		
		char* buffer = new char[cbMessage];
		DWORD num_bytes_read = 0;
		fResult = ReadFile(slot_h, buffer, cbMessage, &num_bytes_read, NULL);

		if (!fResult) 
        { 
            std::ostringstream out;
            out << "ReadFile failed with %d.\n" << GetLastError() << std::endl;
            throw ReadingMailslotException(out.str());
            delete[] buffer; 
            return "";
        } 

        return std::string(buffer);
	}

	MailslotServer::~MailslotServer(){
		CloseHandle(slot_h);
	}




