#ifndef _MAILSLOTCLIENT_
#define _MAILSLOTCLIENT_

#include "common_header.h"

class MailslotClient
{
	std::string path, server;


    struct CreatingFileException : public std::runtime_error{
        CreatingFileException(const std::string& arg): std::runtime_error(arg){}
    };

    struct WritingMailslotException : public std::runtime_error{
        WritingMailslotException(const std::string& arg): std::runtime_error(arg){}
    };
public:

	MailslotClient(std::string server, std::string path);
	
	void setServer(const std::string server);

	void setPath(const std::string path);

	bool sendMessage(std::string message);


};

#endif
