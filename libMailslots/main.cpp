#include "common_header.h"
#include "MailslotClient.h"
#include "MailslotServer.h"

int main(int argc, char** argv)
{
	HANDLE file_h = CreateFile("C:\\Test.txt", GENERIC_WRITE, FILE_SHARE_READ, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if (file_h == INVALID_HANDLE_VALUE){
		std::cout << "Error while creating file num " << GetLastError() << std::endl;
	}
	int i;
	std::string s = "Hello";
	std::string message = "Hello!";
	
	LPDWORD bytes_written = new DWORD;
	if (! WriteFile(file_h, message.c_str(), message.length() + 1, bytes_written, NULL)){
		std::cout << "Error while writing file num " << GetLastError() << std::endl;
	}

	MailslotServer m(".", "C:\\MyMailslot");
	std::cout << m.get_slot() << std::endl;
	MailslotClient c(".", "C:\\MyMailslot");
	c.sendMessage("Hello! Ololo");
	c.sendMessage("hhh");
	//m.~MailslotServer();
	std::cout << m.read_message() << std::endl;
	std::cout << m.read_message() << std::endl;
	std::cin >> i; 
}