#ifndef _MAILSLOTSERVER_
#define _MAILSLOTSERVER_

#include "common_header.h"

class MailslotServer 
{
	HANDLE slot_h;

    struct NotifierException : public std::runtime_error{
        NotifierException(const std::string& arg): std::runtime_error(arg){}
    };

    struct CreatingMailslotException : public std::runtime_error{
        CreatingMailslotException(const std::string& arg): std::runtime_error(arg){}
    };

    struct ReadingMailslotException : public std::runtime_error{
        ReadingMailslotException(const std::string& arg): std::runtime_error(arg){}
    };

public:
	MailslotServer(std::string server, std::string path, DWORD buf_len = 0);
	
	HANDLE get_slot();

    std::string read_message();
	
	~MailslotServer();
};

#endif
