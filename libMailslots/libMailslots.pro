#-------------------------------------------------
#
# Project created by QtCreator 2014-09-13T14:08:52
#
#-------------------------------------------------

QT       -= core gui

TARGET = Mailslots
TEMPLATE = lib
CONFIG += staticlib

SOURCES += \
    mailslot_client.cpp \
    mailslot_server.cpp

HEADERS += \
    common_header.h \
    MailslotClient.h \
    MailslotServer.h
unix {
    target.path = /usr/lib
    INSTALLS += target
}
