#ifndef _MAILSLOTSERVER_
#define _MAILSLOTSERVER_

#include "common_header.h"

class MailslotServer 
{
	HANDLE slot_h;
public:
	MailslotServer(std::string server, std::string path, DWORD buf_len = 0);
	
	HANDLE get_slot();

    std::string read_message();
	
	~MailslotServer();
};

#endif
