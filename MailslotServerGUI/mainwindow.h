#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "MailslotServer.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    MailslotServer* server;
public:

    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:
    void setConnection();

private slots:

    void on_connectButton_clicked();

    void on_sendButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
