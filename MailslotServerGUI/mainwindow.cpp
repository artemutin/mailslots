#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_connectButton_clicked()
{
    if (!ui->connectButton->isChecked){
        QString server_s = ui->serverEdit->text();
        QString path_s = ui->pathEdit->text();
        server = new MailslotServer(server_s.toStdString(), path_s.toStdString());
        ui->sendButton->setEnabled(true);
        ui->connectButton->setText("Disconnect");
        ui->connectButton->isChecked = true;
    }else{
        server->~MailslotServer();
        server = __null;
        ui->serverEdit->setText("");
        ui->pathEdit->setText("");
        ui->connectButton->setText("Connect");
        ui->sendButton->setEnabled(false);
        ui->connectButton->isChecked = false;
    }
}

void MainWindow::on_sendButton_clicked()
{
    QString message = QString::fromStdString(server->read_message());
    ui->messageTextEdit->appendPlainText(message);
}
