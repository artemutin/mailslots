#-------------------------------------------------
#
# Project created by QtCreator 2014-09-13T14:18:04
#
#-------------------------------------------------

QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MailslotServerGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    connectbutton.cpp

HEADERS  += mainwindow.h \
    ../libMailslots/common_header.h \
    ../libMailslots/MailslotServer.h \
    connectbutton.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/release/ -lMailslots
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/debug/ -lMailslots

INCLUDEPATH += $$PWD/../libMailslots
DEPENDPATH += $$PWD/../libMailslots

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/release/libMailslots.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/debug/libMailslots.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/release/Mailslots.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/debug/Mailslots.lib
