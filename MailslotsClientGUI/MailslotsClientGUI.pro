#-------------------------------------------------
#
# Project created by QtCreator 2014-09-14T19:27:12
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MailslotsClientGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h \
    ../libMailslots/MailslotClient.h

FORMS    += mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/release/ -lMailslots
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/debug/ -lMailslots

INCLUDEPATH += $$PWD/../libMailslots
DEPENDPATH += $$PWD/../libMailslots

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += $$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/release/libMailslots.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += $$PWD/../build-libMailslots-Desktop_Qt_5_3_MSVC2010_OpenGL_32bit-Release/debug/libMailslots.a
