#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow), isAddressChanged(true), client(NULL)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_sendMessage_clicked()
{
    QString path = ui->pathEdit->text();
    QString server = ui->serverEdit->text();
    QString message = ui->messageTextEdit->toPlainText();
    if (isAddressChanged){
        delete client;
        client = new MailslotClient(server.toStdString(), path.toStdString());
    }

    client->sendMessage(message.toStdString());
}

void MainWindow::on_serverEdit_textChanged(const QString &arg1)
{
    isAddressChanged = true;
}

void MainWindow::on_pathEdit_textChanged(const QString &arg1)
{
    isAddressChanged = true;
}
