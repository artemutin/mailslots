#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "../libMailslots/MailslotClient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_sendMessage_clicked();

    void on_serverEdit_textChanged(const QString &arg1);

    void on_pathEdit_textChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;
    bool isAddressChanged;
    MailslotClient* client;
};

#endif // MAINWINDOW_H
